SHELL=/bin/bash
# -*- mode: makefile -*-
all: tok

MAIN_OBJS=tok.o
tok: ${MAIN_OBJS}
	-${CLINKER} -o $@ ${MAIN_OBJS} ${PETSC_LIB}
	${RM} -f $(MAIN_OBJS)

NP=4 # 4^NREF  -- override
NREF=1 # override
STEPS=1
ANI=1e-9
TF=2
TSTYPE=arkimex
N=1
TSTOL=6
ORDER=1
TIME=.0002
DT=3e-4
MAJ_RAD=6.2
N_TOK_EX=8
DIM=3

GAMG_COARSE=-mg_coarse_pc_type gamg -mg_coarse_ksp_type cg -mg_coarse_pc_gamg_esteig_ksp_type cg -mg_coarse_pc_gamg_threshold 0.01 -mg_coarse_pc_gamg_coarse_eq_limit 1000 -mg_coarse_pc_gamg_process_eq_limit 100 -mg_coarse_pc_gamg_reuse_interpolation false

PCTYPE=jacobi
SM=jacobi

PCTYPE=gamg
SMOOTHER=-pc_gamg_esteig_ksp_type cg -pc_gamg_coarse_eq_limit 1000 -pc_gamg_process_eq_limit 300 -pc_gamg_reuse_interpolation false -mg_levels_ksp_max_it 1 -mg_levels_sub_pc_type lu -mg_levels_pc_type asm -pc_gamg_asm_use_agg 
#SMOOTHER=-pc_gamg_esteig_ksp_type cg -pc_gamg_coarse_eq_limit 1000 -pc_gamg_process_eq_limit 300 -pc_gamg_reuse_interpolation false -mg_levels_ksp_max_it 1 -mg_levels_sub_pc_type lu -mg_levels_pc_type asm -mg_levels_1_pc_asm_blocks 315 -mg_levels_2_pc_asm_blocks 2006 -mg_levels_3_pc_asm_blocks 6708
SM=asm

PCTYPE=gamg
SMOOTHER=-pc_gamg_esteig_ksp_type cg -pc_gamg_coarse_eq_limit 1000 -pc_gamg_process_eq_limit 300 -pc_gamg_reuse_interpolation false -pc_gamg_recompute_esteig -mg_levels_ksp_max_it 2 -mat_coarsen_typexx hem -mat_coarsen_max_it 4
SM=jac

#PCTYPE=asm
#SMOOTHER=-pc_asm_overlap 0 -sub_pc_type lu # -sub_pc_factor_mat_solver_type mumps
#SM=asm

#PCTYPE=mg
#SMOOTHER=-mg_levels_pc_type asm -mg_levels_pc_asm_overlap 0 -mg_levels_sub_pc_type lu -mg_coarse_pc_type asm -mg_coarse_pc_asm_overlap 0 -mg_coarse_sub_pc_type lu -mg_coarse_ksp_type cg -mg_coarse_ksp_monitor # -mg_coarse_pc_type lu -mg_coarse_pc_factor_mat_solver_type mumps 
#SM=asm
# 2D
#SMOOTHER=-mg_levels_pc_type jacobi -mg_levels_ksp_type chebyshev -mg_coarse_mg_levels_ksp_max_it 2
#SM=jacobi

MAT=-tok_dm_mat_type aijkokkos -tok_dm_vec_type kokkos
#MAT=-dm_mat_type aijhipsparse -dm_vec_type hip

#SOLVER=-ksp_type cg -pc_type lu -pc_factor_mat_solver_type mumps
#SOLVER=-ksp_type cg -pc_type jacobi # -ksp_rtol 1e-12
#SOLVER=-ksp_type cg -pc_type asm -sub_pc_type lu -pc_asm_overlap 0 -sub_pc_factor_mat_solver_type mumps
#SOLVER=-ksp_type cg -pc_type bjkokkos -pc_bjkokkos_ksp_type bicg -pc_bjkokkos_pc_type jacobi -pc_bjkokkos_ksp_rtol 1e-6 -pc_bjkokkos_ksp_max_it 500
SOLVER=-ksp_type cg -ksp_max_it 50000 -pc_type ${PCTYPE} ${SMOOTHER} -ksp_rtol 1e-12 -mg_levels_esteig_ksp_type cg -pc_gamg_threshold 0.05 -pc_gamg_threshold_scale .25 

TSARGS=-ts_type ${TSTYPE} -ts_arkimex_type 1bee -ksp_converged_reason -ts_monitor -ts_max_steps ${STEPS} -ts_max_time ${TIME} -ts_dt ${DT} -ts_adapt_monitor -ts_exact_final_time matchstep -ts_rtol 1e-${TSTOL} 
SOLVER0=-ref1_dm_plex_simplex 0 -ref1_dm_plex_dim 2 -tok_dim ${DIM} -potential_petscspace_degree ${ORDER} -snes_type ksponly -tor_jac_view ascii::ascii_info -tok_theta 0.01 -use_gpu_aware_mpi 0 -tok_anisotropic ${ANI} -petscpartitioner_type simple ${TSARGS} ${SOLVER}

SOLVER2=-ref1_dm_plex_box_faces ${N},${N} ${SOLVER0} ${MAT} -ref1_dm_refine_pre ${NREF} -pc_gamg_aggressive_coarsening 1 -tok_symmetric_source false

PI2 := 6.283185307179586
CIRC := $(shell echo ${PI2}*${MAJ_RAD} | bc)
SOLVER3=-tok_radius_major ${MAJ_RAD} -ref1_dm_plex_box_faces ${N},${N},${N} -ref1_dm_refine_pre ${NREF} -ref_final_dm_refine_hierarchy ${TF} ${SOLVER0} ${MAT} -ref_extrude_dm_extrude ${N_TOK_EX} -ref_extrude_dm_plex_transform_extrude_use_tensor false -ref_extrude_dm_plex_transform_type extrude -ref_extrude_dm_plex_transform_extrude_periodic -ref_extrude_dm_plex_transform_extrude_thickness ${CIRC} -pc_gamg_aggressive_coarsening 2

# -mat_view :A.m:ascii_matlab -

RTYPE=${DIM}D-tstype-${TSTYPE}-refpoloidal-refpol_${NREF}-reftor_${TF}-ntor_${N_TOK_EX}-Q${ORDER}-${PCTYPE}-${SM}
FILE=tok-${RTYPE}
VIEW=-tok_dm_view hdf5:${FILE}.h5 -tok_vec_view hdf5:${FILE}.h5::append -part_0_dm_view hdf5:part_0.h5 -part_0_vec_view hdf5:part_0.h5::append -part_1_dm_view hdf5:part_1.h5 -part_1_vec_view hdf5:part_1.h5::append -part_2_dm_view hdf5:part_2.h5 -part_2_vec_view hdf5:part_2.h5::append -part_3_dm_view hdf5:part_3.h5 -part_3_vec_view hdf5:part_3.h5::append -part_4_dm_view hdf5:part_4.h5 -part_4_vec_view hdf5:part_4.h5::append
# -b0_dm_view hdf5:b0.h5 -b0_vec_view hdf5:b0.h5::append
# VIEW=

run3d:
	-@echo L = ${CIRC}
	-${MPIEXEC} -n ${NP} ${PRE} ./tok ${PRE1} ${SOLVER3} -tok_print ${PRINT} ${VIEW} ${EXTRA} | tee out-${RTYPE}.txt
	-@python3 ${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py ${FILE}.h5
	-@python3 ${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py part_0.h5
#	-@python3 ${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py part_1.h5
#	-@python3 ${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py part_2.h5
	-@python3 ${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py part_3.h5
	-@python3 ${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py part_4.h5

lldb:
	-lldb ./tok -- ${SOLVER3} -tok_print ${PRINT} ${VIEW} ${EXTRA} 

ddt:
	-ddt --connect ${MPIEXEC} -n ${NP} ${PRE} ./tok ${PRE1} ${SOLVER3} -tok_print ${PRINT} ${EXTRA}

run2d:
	-echo ${MPIEXEC} 
	-${MPIEXEC} -n ${NP} ${PRE} ./tok ${PRE1} ${SOLVER2} -tok_print ${PRINT} ${VIEW} -b0_dm_view hdf5:b0_2.h5 -b0_vec_view hdf5:b0_2.h5::append ${EXTRA} | tee out-${RTYPE}.txt
#	-${PRE}                     ./tok ${PRE1} ${SOLVER2} -tok_print ${PRINT} ${VIEW} -b0_dm_view hdf5:b0_2.h5 -b0_vec_view hdf5:b0_2.h5::append ${EXTRA}
	-@${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py b0_2.h5
	-@python3 ${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py part_0.h5
	-@python3 ${PETSC_DIR}/lib/petsc/bin/petsc_gen_xdmf.py ${FILE}.h5

#-cell_dm_view hdf5:fc.h5 -cell_vec_view hdf5:fc.h5::append 

include ${PETSC_DIR}/lib/petsc/conf/variables
include ${PETSC_DIR}/lib/petsc/conf/rules

# ${MPIEXEC} -n ${NP} 
