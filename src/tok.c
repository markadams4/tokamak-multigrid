static char help[] = "Geometric multigrid Poisson solver on Tokamak domain "
                     "with semi-coarsening grid hierarchy\n";

#include <petsc/private/dmimpl.h>
#include <petsc/private/pcmgimpl.h> /*I "petscksp.h" I*/
#include <petscdmplex.h>
#include <petscds.h>
#include <petscmat.h>
#include <petscts.h>

typedef struct {
  PetscInt dim;
  PetscReal print;
  PetscReal last_time;
  /* geometry */
  PetscBool use_360_domains;
  char filename[PETSC_MAX_PATH_LEN];
  /* torus geometry  */
  PetscReal R;
  PetscReal r;
  PetscReal r_inflate;
  /* torus topology */
  PetscInt coarse_toroidal_faces;
  PetscInt toroidal_refine;
  PetscInt poloidal_refine;
  PetscInt uniform_poloidal_refine;
  PetscInt nlevels;
  /* phsyics */
  PetscBool anisotropic;
  PetscReal anisotropic_eps;
  /* init Maxwellian */
  PetscReal theta;
  PetscReal n;
  PetscReal source_location[3];
  PetscBool sym_source;
  /* cache */
  PetscInt sequence_number;
  PetscLogEvent event[10];
} AppCtx;

/* q: safty factor */
#define qsafty(__psi) (3. * pow(__psi, 2.0))
static PetscReal s_r_major;
static PetscReal s_r_minor;

static PetscErrorCode ProcessOptions(MPI_Comm comm, AppCtx *ctx) {
  PetscMPIInt size;
  PetscInt nn;
  PetscFunctionBeginUser;
  PetscCallMPI(MPI_Comm_size(comm, &size));
  ctx->dim = 3; // 2 is for plane solve (debugging)
  /* mesh */
  ctx->R = 6.2;
  ctx->r = 2.0;
  ctx->r_inflate = 1;
  ctx->coarse_toroidal_faces = 1;
  ctx->toroidal_refine = 0;
  ctx->poloidal_refine = 1;
  ctx->uniform_poloidal_refine = 1;
  ctx->use_360_domains = PETSC_FALSE;
  ctx->sym_source = PETSC_TRUE;
  ctx->anisotropic = PETSC_FALSE;
  ctx->anisotropic_eps = 1.0; // default to no anisotropy (debug)
  ctx->n = 1;
  ctx->theta = .05;
  ctx->print = 1; // print time period
  ctx->source_location[0] = 0;
  ctx->source_location[1] = 1.4;
  ctx->source_location[2] = ctx->R;

  PetscOptionsBegin(comm, "tok_", "Tokamak solver", "DMPLEX");
  nn = 3;
  PetscCall(PetscOptionsRealArray("-source_location",
                                  "Maxwellian source location", "tok.c",
                                  ctx->source_location, &nn, NULL));
  PetscCall(PetscOptionsInt("-toroidal_refine",
                            "Number of refinement steps in toroidal direction "
                            "(new levels, semi-coarsening)",
                            "tok.c", ctx->toroidal_refine,
                            &ctx->toroidal_refine, NULL));
  PetscCall(PetscOptionsInt("-dim",
                            "The dimension of problem (2 is for debugging)",
                            "tok.c", ctx->dim, &ctx->dim, NULL));
  PetscCheck(ctx->dim == 2 || ctx->dim == 3, comm, PETSC_ERR_ARG_WRONG,
             "dim (%d) != 2 or 3", (int)ctx->dim);
  if (ctx->dim == 3) {
    ctx->coarse_toroidal_faces = 4; // 2 for SC model
    PetscCall(PetscOptionsInt(
        "-coarse_toroidal_faces", "Number of planes for mesh", "tok.c",
        ctx->coarse_toroidal_faces, &ctx->coarse_toroidal_faces, NULL));
    PetscCheck(ctx->coarse_toroidal_faces % size == 0 ||
                   size % ctx->coarse_toroidal_faces % size == 0,
               comm, PETSC_ERR_ARG_WRONG,
               "Number of coarse face (%d) mod num procs %d != 0",
               (int)ctx->coarse_toroidal_faces, (int)size);
  } else {
    ctx->coarse_toroidal_faces = 1;
    ctx->source_location[2] = 0;
  }
  PetscCall(PetscOptionsInt(
      "-poloidal_refine",
      "Number of refinement steps in poloidal plane (new levels)", "tok.c",
      ctx->poloidal_refine, &ctx->poloidal_refine, NULL));
  PetscCall(PetscOptionsInt(
      "-uniform_poloidal_refine",
      "Uniform poloidal plane refinement levels are created", "tok.c",
      ctx->uniform_poloidal_refine, &ctx->uniform_poloidal_refine, NULL));
  PetscCall(PetscOptionsReal(
      "-print", "Print time period, 0 for no printing, -1 for first and last",
      "tok.c", ctx->print, &ctx->print, NULL));
  ctx->last_time = -ctx->print; // cache
  PetscCall(PetscOptionsBool(
      "-use_360_domains", "Use processor domains around the whole torus",
      "tok.c", ctx->use_360_domains, &ctx->use_360_domains, NULL));
  PetscCall(PetscOptionsBool("-symmetric_source",
                             "Add two sources with opposite sign", "tok.c",
                             ctx->sym_source, &ctx->sym_source, NULL));
  PetscCall(PetscOptionsReal("-anisotropic", "Anisotropic epsilon", "tok.c",
                             ctx->anisotropic_eps, &ctx->anisotropic_eps,
                             &ctx->anisotropic));
  ctx->nlevels = ctx->poloidal_refine + ctx->toroidal_refine + 1;
  /* Domain and mesh definition */
  PetscCall(PetscOptionsReal("-radius_minor", "Minor radius of torus", "tok.c",
                             ctx->r, &ctx->r, NULL));
  PetscCall(PetscOptionsReal("-radius_major", "Major radius of torus", "tok.c",
                             ctx->R, &ctx->R, NULL));
  PetscCall(PetscOptionsReal("-radius_inflation",
                             "inflate domain factor from minor radius", "tok.c",
                             ctx->r_inflate, &ctx->r_inflate, NULL));
  ctx->filename[0] = '\0';
  PetscCall(PetscOptionsString(
      "-file", "2D mesh file on [0, 1]^2 and scaled by 2 * r_minor", "tok.c",
      ctx->filename, ctx->filename, sizeof(ctx->filename), NULL));
  PetscCall(
      PetscOptionsReal("-n", "Maxwellian n", "tok.c", ctx->n, &ctx->n, NULL));
  PetscCall(PetscOptionsReal("-theta", "Maxwellian kT/m", "tok.c", ctx->theta,
                             &ctx->theta, NULL));
  PetscOptionsEnd();
  s_r_major = ctx->R;
  s_r_minor = ctx->r;

  PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode Monitor(TS ts, PetscInt stepi, PetscReal time, Vec X,
                       void *actx) {
  TSConvergedReason reason;
  AppCtx *ctx = (AppCtx *)actx;
  DM dm;
  PetscCall(VecGetDM(X, &dm));

  PetscFunctionBeginUser;
  PetscCall(TSGetConvergedReason(ts, &reason));
  if (ctx->print != 0.0 &&
      ((reason || stepi == 0) ||
       (ctx->print > 0 && (time - ctx->last_time >= ctx->print)))) {
    PetscCall(PetscInfo(dm, "%d) vec view. sequence number %d\n", (int)stepi,
                        (int)ctx->sequence_number));
    PetscCall(DMSetOutputSequenceNumber(dm, ctx->sequence_number, time));
    ctx->sequence_number++;
    PetscCall(PetscLogEventBegin(ctx->event[5], 0, 0, 0, 0));
    PetscCall(VecViewFromOptions(X, NULL, "-tok_vec_view"));
    PetscCall(PetscLogEventEnd(ctx->event[5], 0, 0, 0, 0));
    ctx->last_time = time;
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/* Simple shift to origin */
static PetscErrorCode OriginShift2D(DM dm, const PetscReal R_0, AppCtx *ctx) {
  Vec coordinates;
  PetscScalar *coords;
  PetscInt N;

  PetscFunctionBeginUser;
  PetscCall(DMGetCoordinatesLocal(dm, &coordinates));
  PetscCall(VecGetSize(coordinates, &N));
  PetscCall(VecGetArrayWrite(coordinates, &coords));
  // shift coordinates to center on (R,0). Assume the domain is (0,1)^2
  for (int ii = 0; ii < N; ii += 2) {
    PetscScalar *v = &coords[ii];
    v[0] *= 2 * ctx->r * ctx->r_inflate;
    v[1] *= 2 * ctx->r * ctx->r_inflate;
    v[0] += R_0 - ctx->r * ctx->r_inflate;
    v[1] += -ctx->r * ctx->r_inflate;
  }
  PetscCall(VecRestoreArrayWrite(coordinates, &coords));
  PetscCall(DMSetCoordinates(dm, coordinates));
  PetscFunctionReturn(PETSC_SUCCESS);
}

// store Cartesian (X,Y,Z) for plotting 3D, (X,Y) for 2D - shift R_0 onto torus
// (psi,theta,phi) --> (X,Y,Z)
#define cylToCart(__R_0, __psi, __theta, __phi, __cart)                        \
  {                                                                            \
    PetscReal __R = (__R_0) + (__psi) * PetscCosReal(__theta);                 \
    __cart[0] = __R * PetscCosReal(__phi);                                     \
    __cart[1] = __psi * PetscSinReal(__theta);                                 \
    __cart[2] = -__R * PetscSinReal(__phi);                                    \
  }

/* coordinate transformation - simple radial coordinates. Not really cylindrical
 * as r_Minor is radius from plane axis */
#define XYToPsiTheta(__x, __y, __psi, __theta)                                 \
  {                                                                            \
    __psi = PetscSqrtReal((__x) * (__x) + (__y) * (__y));                      \
    if (__psi < PETSC_SQRT_MACHINE_EPSILON)                                    \
      __theta = 0.;                                                            \
    else {                                                                     \
      __theta = PetscAtan2Real(__y, __x);                                      \
    }                                                                          \
  }

#define CartTocyl2D(__R_0, __R, __cart, __psi, __theta)                        \
  {                                                                            \
    __R = __cart[0];                                                           \
    XYToPsiTheta(__R - __R_0, __cart[1], __psi, __theta);                      \
  }

#define CartTocyl3D(__R_0, __R, __cart, __psi, __theta, __phi)                 \
  {                                                                            \
    __R = PetscSqrtReal(__cart[0] * __cart[0] + __cart[2] * __cart[2]);        \
    if (__R < PETSC_SQRT_MACHINE_EPSILON)                                      \
      __phi = 0;                                                               \
    else {                                                                     \
      __phi = PetscAtan2Real(-__cart[2], __cart[0]);                           \
      if (__phi < 0)                                                           \
        __phi += 2. * PETSC_PI;                                                \
    }                                                                          \
    XYToPsiTheta(__R - __R_0, __cart[1], __psi, __theta);                      \
  }

static void f1_u(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[],
                 const PetscScalar u[], const PetscScalar u_t[],
                 const PetscScalar u_x[], const PetscInt aOff[],
                 const PetscInt aOff_x[], const PetscScalar a[],
                 const PetscScalar a_t[], const PetscScalar a_x[], PetscReal t,
                 const PetscReal x[], PetscInt numConstants,
                 const PetscScalar constants[], PetscScalar f1[]) {
  PetscInt d;
  for (d = 0; d < dim; ++d)
    f1[d] = u_x[d];
}

static void
g3_uu(PetscInt dim, PetscInt Nf, PetscInt NfAux, const PetscInt uOff[],
      const PetscInt uOff_x[], const PetscScalar u[], const PetscScalar u_t[],
      const PetscScalar u_x[], const PetscInt aOff[], const PetscInt aOff_x[],
      const PetscScalar a[], const PetscScalar a_t[], const PetscScalar a_x[],
      PetscReal t, PetscReal u_tShift, const PetscReal x[],
      PetscInt numConstants, const PetscScalar constants[], PetscScalar g3[]) {
  PetscInt d;
  for (d = 0; d < dim; ++d)
    g3[d * dim + d] = 1.0;
}

static void g0_u(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                 const PetscInt uOff[], const PetscInt uOff_x[],
                 const PetscScalar u[], const PetscScalar u_t[],
                 const PetscScalar u_x[], const PetscInt aOff[],
                 const PetscInt aOff_x[], const PetscScalar a[],
                 const PetscScalar a_t[], const PetscScalar a_x[], PetscReal t,
                 PetscReal u_tShift, const PetscReal x[], PetscInt numConstants,
                 const PetscScalar constants[], PetscScalar g0[]) {
  g0[0] = u_tShift * 1.0;
}

// compute unit vector along field line -- used for debugging, same code as
// anisotropicg3!
#define FD_DIR (-1)
static PetscErrorCode b_vec(PetscInt dim, PetscReal time, const PetscReal x[],
                            PetscInt Nf, PetscScalar *u, void *actx) {
  const PetscReal *x_vec = x, dt = PETSC_SQRT_MACHINE_EPSILON, vpar = 1;
  PetscReal dphi, qsaf, theta, psi, R_xz, phi = 0.0, xprime_vec[3], len = 0;
  // push coordinate along field line and do FD to get vector b
  PetscFunctionBegin;
  if (dim == 2) {
    CartTocyl2D(0, R_xz, x_vec, psi, theta);
    R_xz = PetscSqrtReal(x_vec[0] * x_vec[0] * x_vec[1] * x_vec[1]);
    phi = 0.0; // bring to plane
  } else {
    CartTocyl3D(s_r_major, R_xz, x_vec, psi, theta, phi);
  }
  dphi = dt * vpar / R_xz; // the push, use R_0 for 2D also
  qsaf = qsafty(psi / s_r_minor);
  theta += FD_DIR * qsaf * dphi; // little twist
  phi += FD_DIR * dphi;
  cylToCart(((dim == 3) ? s_r_major : 0), psi, theta, phi,
            xprime_vec); // don't shift 2D
  // make vector and return it
  for (PetscInt i = 0; i < dim; i++) {
    u[i] = (xprime_vec[i] - x_vec[i]) / dt;
    len += u[i] * u[i];
  }
  len = PetscSqrtReal(len);
  for (int ii = 0; ii < dim; ii++)
    u[ii] *= 1 / len;

  PetscFunctionReturn(PETSC_SUCCESS);
}

#define CROSS3(__a, __b, __v)                                                  \
  {                                                                            \
    __v[0] = __a[1] * __b[2] - __a[2] * __b[1];                                \
    __v[1] = __a[2] * __b[0] - __a[0] * __b[2];                                \
    __v[2] = __a[0] * __b[1] - __a[1] * __b[0];                                \
  }

static char s_stage = '0';
static void anisotropicg3(const PetscInt dim, const PetscReal uu[],
                          const PetscReal xx[], const PetscInt numConstants,
                          const PetscScalar constants[], PetscScalar g3[]) {
  PetscInt ii;
  PetscReal x_vec[3] = {0, 0, 0}, bb[3] = {0, 0, 0}, aa[] = {0, 0, 0}, xdot = 0,
            vv[] = {0, 0, 0}, qsaf, theta, psi, R_xz, phi, cc, ss, RR[3][3],
            fact, det;
  PetscReal invR[3][3], adjA[3][3],
      vx[3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}},
      vx2[3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}}, dphi;
  const PetscReal dt = PETSC_SQRT_MACHINE_EPSILON,
                  (*DD)[3][3] = (PetscReal(*)[3][3])constants,
                  vpar = 1; // the push, use R_0 for 2D also
  PetscBool print = 0;

  // get FD \hat b_0 vector
  for (ii = 0; ii < dim; ii++)
    x_vec[ii] = xx[ii]; // copy into padded vec for 2D
  if (dim == 2) {
    CartTocyl2D(0, R_xz, x_vec, psi, theta);
    R_xz = PetscSqrtReal(x_vec[0] * x_vec[0] * x_vec[1] * x_vec[1]);
    phi = 0.0; // bring to plane
  } else {
    CartTocyl3D(s_r_major, R_xz, x_vec, psi, theta, phi);
  }
  if (psi < PETSC_SQRT_MACHINE_EPSILON) {
    for (PetscInt d = 0; d < dim; ++d)
      g3[d * dim + d] = 1;
    // printf("** |b| = 0 ** *********** origin: x = %e %e  ***********\nD = %e
    // %e \n    %e %e\n", x_vec[0], x_vec[1], g3[0], g3[1], g3[2], g3[3]);
    return;
  }
  dphi = dt * vpar / R_xz; // the push, use R_0 for 2D also
  qsaf = qsafty(psi / s_r_minor);
  theta += FD_DIR * qsaf * dphi; // little twist
  phi += FD_DIR * dphi;          // 2D has fake twist
  cylToCart(((dim == 3) ? s_r_major : 0), psi, theta, phi,
            aa); // don't shift 2D
  for (ii = 0, xdot = 0; ii < dim; ii++) {
    bb[ii] = (aa[ii] - x_vec[ii]);
    xdot += bb[ii] * bb[ii];
  }
  xdot = 1 / PetscSqrtReal(xdot);
  for (ii = 0; ii < dim; ii++)
    bb[ii] *= xdot;
  // make unit vector \hat phi
  aa[0] = 0; // direction of strong conductivity (y or z)
  if (dim == 2)
    aa[1] = 1;
  else {
    aa[1] = 0;
    aa[2] = 1;
  }
  // Let v = a x b
  CROSS3(aa, bb, vv);
  // get rotation matrix R
  for (ii = 0, cc = 0; ii < 3; ii++)
    cc += aa[ii] * bb[ii];
  for (ii = 0, ss = 0; ii < 3; ii++)
    ss += vv[ii] * vv[ii];
  ss = PetscSqrtReal(ss);
  fact = 1 / (1 + cc);
  // ratation matrix
  vx[0][1] = -vv[2];
  vx[1][0] = vv[2];
  if (dim == 3) {
    vx[0][2] = vv[1];
    vx[2][0] = -vv[1];
    vx[1][2] = -vv[0];
    vx[2][1] = vv[0];
  }
  for (PetscInt i = 0; i < dim; ++i)
    for (PetscInt j = 0; j < dim; ++j)
      for (PetscInt k = 0; k < dim; ++k)
        vx2[i][k] += vx[i][j] * vx[j][k];
  for (PetscInt i = 0; i < dim; ++i) {
    for (PetscInt j = 0; j < dim; ++j) {
      if (i == j)
        RR[i][j] = 1;
      else
        RR[i][j] = 0;
      RR[i][j] += vx[i][j];
      RR[i][j] += fact * vx2[i][j];
    }
  }
  // inverse of R
  if (dim == 2) {
    /* Calculate determinant of matrix A */
    det = (RR[0][0] * RR[1][1]) - (RR[0][1] * RR[1][0]);
    /* Find adjoint of matrix RR */
    adjA[0][0] = RR[1][1];
    adjA[1][1] = RR[0][0];
    adjA[0][1] = -RR[0][1];
    adjA[1][0] = -RR[1][0];
    for (PetscInt i = 0; i < 2; i++)
      for (PetscInt j = 0; j < 2; j++) {
        invR[i][j] = adjA[i][j] / det;
        g3[i * dim + j] = 0;
      }
  } else {
    /*  // inverse RR */
    det = 0;
    for (PetscInt i = 0; i < 3; i++)
      det = det + (RR[0][i] * (RR[1][(i + 1) % 3] * RR[2][(i + 2) % 3] -
                               RR[1][(i + 2) % 3] * RR[2][(i + 1) % 3]));
    for (PetscInt i = 0; i < 3; i++) {
      for (PetscInt j = 0; j < 3; j++)
        invR[i][j] =
            ((RR[(j + 1) % 3][(i + 1) % 3] * RR[(j + 2) % 3][(i + 2) % 3]) -
             (RR[(j + 1) % 3][(i + 2) % 3] * RR[(j + 2) % 3][(i + 1) % 3])) /
            det;
    }
  }
  // R D R^-1
  for (PetscInt i = 0; i < dim; ++i) {
    for (PetscInt j = 0, dj = (dim == 2) ? 1 : 0; j < dim; ++j, dj++) {
      // double tt = 0;
      for (PetscInt k = 0, dk = (dim == 2) ? 1 : 0; k < dim; ++k, dk++) {
        for (PetscInt q = 0; q < dim; ++q)
          g3[i * dim + q] += RR[i][j] * (*DD)[dj][dk] * invR[k][q];
      }
    }
  }
  if (print && dim == 3)
    printf("D = %e %e %e\n    %e %e %e\n    %e %e %e\n", g3[0], g3[1], g3[2],
           g3[3], g3[4], g3[5], g3[6], g3[7], g3[8]);
  if (print && dim == 2)
    printf("D = %e %e\n    %e %e\n", g3[0], g3[1], g3[2], g3[3]);
}

static void g3_anisotropic(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                           const PetscInt uOff[], const PetscInt uOff_x[],
                           const PetscScalar u[], const PetscScalar u_t[],
                           const PetscScalar u_x[], const PetscInt aOff[],
                           const PetscInt aOff_x[], const PetscScalar a_a[],
                           const PetscScalar a_t[], const PetscScalar a_x[],
                           PetscReal t, PetscReal u_tShift, const PetscReal x[],
                           PetscInt numConstants, const PetscScalar constants[],
                           PetscScalar g3[]) {
  s_stage = '3';
  anisotropicg3(dim, u, x, numConstants, constants, g3);
}

static void f1_anisotropic(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                           const PetscInt uOff[], const PetscInt uOff_x[],
                           const PetscScalar u[], const PetscScalar u_t[],
                           const PetscScalar u_x[], const PetscInt aOff[],
                           const PetscInt aOff_x[], const PetscScalar a[],
                           const PetscScalar a_t[], const PetscScalar a_x[],
                           PetscReal t, const PetscReal x[],
                           PetscInt numConstants, const PetscScalar constants[],
                           PetscScalar f1[]) {
  PetscScalar g3[9];
  for (PetscInt i = 0; i < 9; i++)
    g3[i] = 0;
  s_stage = '1';
  anisotropicg3(dim, u, x, numConstants, constants, g3);

  for (PetscInt i = 0; i < dim; ++i) {
    f1[i] = 0;
    for (PetscInt j = 0; j < dim; ++j) {
      f1[i] += g3[i * dim + j] * u_x[j];
    }
  }
}

static void f0_dt(PetscInt dim, PetscInt Nf, PetscInt NfAux,
                  const PetscInt uOff[], const PetscInt uOff_x[],
                  const PetscScalar u[], const PetscScalar u_t[],
                  const PetscScalar u_x[], const PetscInt aOff[],
                  const PetscInt aOff_x[], const PetscScalar a[],
                  const PetscScalar a_t[], const PetscScalar a_x[], PetscReal t,
                  const PetscReal x[], PetscInt numConstants,
                  const PetscScalar constants[], PetscScalar f0[]) {
  f0[0] = u_t[0];
}

static PetscErrorCode u_zero(PetscInt dim, PetscReal time, const PetscReal x[],
                             PetscInt Nc, PetscScalar *u, void *ctx) {
  u[0] = 0.0;
  return PETSC_SUCCESS;
}

static PetscErrorCode SetupProblem(DM dm, AppCtx *ctx) {
  PetscDS ds;
  DMLabel label;
  // const PetscInt id = 1;

  PetscFunctionBeginUser;
  PetscCall(DMGetDS(dm, &ds));
  if (ctx->anisotropic) {
    PetscReal eps[3][3] = {
        {ctx->anisotropic_eps, 0, 0}, {0, ctx->anisotropic_eps, 0}, {0, 0, 1}};
    PetscCall(PetscDSSetConstants(ds, 9, (PetscReal *)eps));
  }
  PetscCall(DMGetLabel(dm, "marker", &label));
  if (ctx->anisotropic) {
    PetscCall(PetscDSSetJacobian(ds, 0, 0, g0_u, NULL, NULL, g3_anisotropic));
    PetscCall(PetscDSSetResidual(ds, 0, f0_dt, f1_anisotropic));
  } else {
    PetscCall(PetscDSSetJacobian(ds, 0, 0, g0_u, NULL, NULL, g3_uu));
    PetscCall(PetscDSSetResidual(ds, 0, f0_dt, f1_u));
  }

  PetscCall(PetscDSSetExactSolution(ds, 0, u_zero, ctx));
  PetscCall(PetscDSSetExactSolutionTimeDerivative(ds, 0, u_zero, ctx));
  // PetscCall(DMAddBoundary(dm, DM_BC_ESSENTIAL, "wall", label, 1, &id, 0, 0,
  // NULL, (void (*)(void))u_zero, NULL, ctx, NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

static PetscErrorCode SetupDiscretization(DM dm, const char name[],
                                          AppCtx *ctx) {
  PetscFE fe;
  PetscBool simplex = PETSC_FALSE;
  PetscInt dim;
  char prefix[PETSC_MAX_PATH_LEN];
  DM cdm = dm;

  PetscFunctionBeginUser;
  PetscCall(DMGetDimension(dm, &dim));
  PetscCheck(dim == ctx->dim, PetscObjectComm((PetscObject)dm),
             PETSC_ERR_ARG_WRONG, "Initial DM dim (%d) != ctx (%d)", (int)dim,
             (int)ctx->dim);
  PetscCall(PetscSNPrintf(prefix, PETSC_MAX_PATH_LEN, "%s_", name));
  PetscCall(DMPlexIsSimplex(dm, &simplex));
  PetscCheck(!simplex, PetscObjectComm((PetscObject)dm), PETSC_ERR_ARG_WRONG,
             "simplex");
  PetscCall(PetscFECreateDefault(PetscObjectComm((PetscObject)dm), dim, 1,
                                 simplex, name ? prefix : NULL, -1, &fe));
  PetscCall(PetscObjectSetName((PetscObject)fe, name));
  PetscCall(PetscFEViewFromOptions(fe, NULL, "-fe_view"));
  PetscCall(DMSetField(dm, 0, NULL, (PetscObject)fe));
  PetscCall(DMCreateDS(dm));
  PetscCall(SetupProblem(dm, ctx));
  while (cdm) {
    PetscCall(DMCopyDisc(dm, cdm));
    PetscCall(DMGetCoarseDM(cdm, &cdm));
  }
  PetscCall(PetscFEDestroy(&fe));
  PetscFunctionReturn(PETSC_SUCCESS);
}

static PetscErrorCode proc_func(PetscInt dim, PetscReal time,
                                const PetscReal x[], PetscInt Nf_dummy,
                                PetscScalar *u, void *actx) {
  PetscFunctionBegin;
  u[0] = time;
  PetscFunctionReturn(PETSC_SUCCESS);
}

static PetscErrorCode view_partitions(DM dm, const char prefix[], AppCtx *ctx) {
  PetscFunctionBegin;
  if (ctx->print) { // view partitions
    DM cdm = dm;
    PetscInt r = 0;
    MPI_Comm comm = PetscObjectComm((PetscObject)dm);
    PetscErrorCode (*initu[1])(PetscInt, PetscReal, const PetscReal[], PetscInt,
                               PetscScalar[], void *);
    DM celldm;
    PetscFE fe;
    Vec uu;
    PetscMPIInt rank;
    initu[0] = proc_func;
    PetscCallMPI(MPI_Comm_rank(comm, &rank));
    PetscCall(
        PetscFECreateDefault(comm, ctx->dim, 1, PETSC_FALSE, "part_", -1, &fe));
    PetscCall(PetscObjectSetName((PetscObject)fe, "rank"));
    PetscCall(PetscFEViewFromOptions(fe, NULL, "-fe_view"));
    while (cdm) {
      char argstr1[256] = "-part_0";
      char argstr2[256] = "-part_0";
      if (r > 9) {
        argstr1[6] = 'A' + r - 10;
        argstr2[6] = 'A' + r - 10;
      } else {
        argstr1[6] = '0' + r;
        argstr2[6] = '0' + r;
      }
      r++;
      PetscCall(DMClone(cdm, &celldm));
      PetscCall(DMSetField(celldm, 0, NULL, (PetscObject)fe));
      PetscCall(DMCreateDS(celldm));
      PetscCall(DMCreateGlobalVector(celldm, &uu));
      PetscCall(PetscObjectSetName((PetscObject)uu, "uu"));
      PetscCall(DMProjectFunction(celldm, (PetscReal)rank + 1, initu, NULL,
                                  INSERT_ALL_VALUES, uu));
      // PetscCall(PetscStrcat(argstr1, prefix));
      PetscCall(PetscStrcat(argstr1, "_dm_view"));
      PetscCall(DMViewFromOptions(celldm, NULL, argstr1));
      // PetscCall(PetscStrcat(argstr2, prefix));
      PetscCall(PetscStrcat(argstr2, "_vec_view"));
      PetscCall(VecViewFromOptions(uu, NULL, argstr2));
      PetscCall(PetscInfo(dm, "arg strings for part view = %s %s\n", argstr1,
                          argstr2));
      PetscCall(DMDestroy(&celldm));
      PetscCall(VecDestroy(&uu));
      PetscCall(DMGetCoarseDM(cdm, &cdm));
    }
    PetscCall(PetscFEDestroy(&fe));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

// create Meshes. Read file or create 2D with refine & distribute (ref1), refine
// (ref2), extrude (ref_extrude), refine (ref_final)
static PetscErrorCode CreateMeshes(MPI_Comm comm, AppCtx *ctx, DM *a_dm) {
  size_t len;
  DM dm;

  PetscFunctionBegin;
  // coarse grid
  PetscCall(PetscStrlen(ctx->filename, &len));
  if (len) {
    PetscInt dim;
    PetscCall(DMPlexCreateFromFile(comm, ctx->filename, "torus_plex",
                                   PETSC_FALSE, &dm));
    PetscCall(DMGetDimension(dm, &dim));
    PetscCheck(dim == 2, comm, PETSC_ERR_ARG_WRONG, "Initial DM dim (%d) != 2",
               (int)dim);
  } else {
    PetscCall(DMCreate(comm, &dm));
    PetscCall(DMSetType(dm, DMPLEX));
    PetscCall(DMPlexDistributeSetDefault(
        dm, PETSC_TRUE)); // distribute initial refinement (one cell per proc)
    PetscCall(PetscObjectSetOptionsPrefix((PetscObject)dm, "ref1_"));
    PetscCall(DMSetFromOptions(dm)); // for global DM params -- matrix type
    PetscCall(DMViewFromOptions(dm, NULL, "-dm_view")); // ref1_dm_view
    PetscCall(DMPlexDistributeSetDefault(
        dm, PETSC_FALSE)); // uniform refinement w/o distribute
    PetscCall(PetscObjectSetOptionsPrefix((PetscObject)dm, "ref2_"));
    PetscCall(DMSetFromOptions(dm));      // refine w/o distribute
    PetscCall(DMLocalizeCoordinates(dm)); // periodic
    PetscCall(PetscObjectSetName((PetscObject)dm, "Coarse Mesh"));
    PetscCall(DMViewFromOptions(dm, NULL, "-dm_view")); // ref2_dm_view
  }
  PetscCall(DMSetApplicationContext(dm, ctx));
  { // print safety factor at source location
    PetscScalar *v = &ctx->source_location[0], theta, psi, R, phi;
    CartTocyl3D(ctx->R, R, v, psi, theta, phi);
    PetscCall(PetscInfo(dm, "qsafty(x_source) = %e  theta(x_source) = %e\n",
                        qsafty(psi / ctx->r), theta));
  }
  // shift
  PetscCall(OriginShift2D(dm, ctx->dim == 3 ? ctx->R : 0, ctx));
  // create fine grids - extrude
  if (ctx->dim == 3) {
    Vec coordinates, coordinates2;
    PetscScalar *coords, R_0 = ctx->R;
    PetscInt N;
    PetscCall(DMPlexDistributeSetDefault(
        dm, PETSC_TRUE)); // extrude to fill all procs
    PetscCall(PetscObjectSetOptionsPrefix((PetscObject)dm, "ref_extrude_"));
    PetscCall(DMSetFromOptions(dm));
    PetscCall(DMViewFromOptions(
        dm, NULL, "-dm_view_pre")); // ref_extrude_dm_view_pre hdf5:part_4.h5
    PetscCall(DMViewFromOptions(dm, NULL, "-dm_view_pre_aux")); // debug
    // wrap around torus axis
    PetscCall(DMGetCoordinatesLocalSetUp(dm));
    PetscCall(DMGetCoordinatesLocal(dm, &coordinates));
    PetscCall(DMGetCoordinates(dm, &coordinates2));
    PetscCall(VecGetLocalSize(coordinates, &N));
    PetscCall(VecGetArrayWrite(coordinates, &coords));
    for (int ii = 0; ii < N; ii += 3) {
      PetscScalar *v = &coords[ii], theta, psi, R, phi, Z;
      CartTocyl2D(R_0, R, v, psi, theta);
      Z = v[2], phi = Z / R_0; // Z: 0 : 2pi * R_0
      cylToCart(R_0, psi, theta, phi, v);
    }
    PetscCall(VecRestoreArrayWrite(coordinates, &coords));
    PetscCall(DMGetCoordinatesLocalSetUp(dm));
    PetscCall(DMViewFromOptions(dm, NULL, "-dm_view")); // ref_extrude_dm_view
  }
  // final refine
  PetscCall(DMPlexDistributeSetDefault(
      dm, PETSC_FALSE)); // not really needed as should be full and balanced now
  PetscCall(PetscObjectSetOptionsPrefix((PetscObject)dm, "ref_final_"));
  PetscCall(DMSetFromOptions(dm));
  // cleanup
  PetscCall(PetscObjectSetOptionsPrefix((PetscObject)dm, NULL));
  // view parts
  PetscCall(view_partitions(dm, "part", ctx));
  *a_dm = dm;
  PetscFunctionReturn(PETSC_SUCCESS);
}

static PetscErrorCode maxwellian(PetscInt dim, PetscReal time,
                                 const PetscReal x[], PetscInt Nf_dummy,
                                 PetscScalar *u, void *actx) {
  AppCtx *ctx = (AppCtx *)actx;
  PetscInt i, kk;
  PetscReal v2 = 0, theta = ctx->theta, factor,
            shift[3] = {0, 0, 0}; /* theta = 2kT/mc^2 */
  PetscFunctionBegin;
  for (kk = 0, factor = 1; kk < (ctx->sym_source ? 2 : 1); kk++, factor = -1) {
    /* evaluate the Maxwellian */
    for (i = 0; i < dim; ++i)
      shift[i] = factor * ctx->source_location[i];
    for (i = 0, v2 = 0; i < dim; ++i)
      v2 += (x[i] - shift[i]) * (x[i] - shift[i]);
    u[0] += factor * ctx->n * PetscPowReal(PETSC_PI * theta, -1.5) *
            PetscExpReal(-v2 / theta);
    /* evaluate the Maxwellian (negative density) */
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

int main(int argc, char **argv) {
  AppCtx actx, *ctx = &actx, *mctxs[1] = {ctx}; /* work context */
  DM dm;
  SNES snes;
  PC pc;
  TS ts;
  KSP ksp;
  Vec u;
  char name[] = "potential";
  PetscErrorCode (*initu[1])(PetscInt, PetscReal, const PetscReal[], PetscInt,
                             PetscScalar[], void *);

  PetscFunctionBeginUser;
  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  PetscCall(ProcessOptions(PETSC_COMM_WORLD, ctx));
  PetscCall(PetscLogEventRegister(",CreateHierarchy", PETSC_OBJECT_CLASSID,
                                  &ctx->event[0]));
  PetscCall(PetscLogEventRegister("  ,CreateHier 1", PETSC_OBJECT_CLASSID,
                                  &ctx->event[7]));
  PetscCall(PetscLogEventRegister("  ,CreateHier 2", PETSC_OBJECT_CLASSID,
                                  &ctx->event[8]));
  PetscCall(
      PetscLogEventRegister("    ,View", PETSC_OBJECT_CLASSID, &ctx->event[6]));
  PetscCall(PetscLogEventRegister("    ,Distribute", PETSC_OBJECT_CLASSID,
                                  &ctx->event[4]));
  PetscCall(PetscLogEventRegister("  ,View parts", PETSC_OBJECT_CLASSID,
                                  &ctx->event[9]));
  PetscCall(PetscLogEventRegister(",Solver setup", PETSC_OBJECT_CLASSID,
                                  &ctx->event[1]));
  PetscCall(
      PetscLogEventRegister(",TSSolve", PETSC_OBJECT_CLASSID, &ctx->event[2]));
  PetscCall(PetscLogEventRegister(",Post solve", PETSC_OBJECT_CLASSID,
                                  &ctx->event[3]));
  PetscCall(
      PetscLogEventRegister(",View", PETSC_OBJECT_CLASSID, &ctx->event[5]));
  /* Create Plex - serial DM for serial coarse grid */
  PetscCallMPI(MPI_Barrier(MPI_COMM_WORLD));
  PetscCall(PetscLogEventBegin(ctx->event[0], 0, 0, 0, 0));
  PetscCall(CreateMeshes(PETSC_COMM_WORLD, ctx, &dm));
  PetscCall(PetscLogEventEnd(ctx->event[0], 0, 0, 0, 0));
  /* Setup problem */
  PetscCall(PetscLogEventBegin(ctx->event[1], 0, 0, 0, 0));
  PetscCall(SetupDiscretization(dm, name, ctx));
  PetscCall(DMPlexCreateClosureIndex(dm, NULL)); // performance
  PetscCall(PetscLogEventEnd(ctx->event[1], 0, 0, 0, 0));
  PetscCall(PetscLogEventBegin(ctx->event[5], 0, 0, 0, 0));
  PetscCall(DMViewFromOptions(dm, NULL, "-tok_dm_view_aux"));
  PetscCall(DMViewFromOptions(dm, NULL, "-tok_dm_view"));
  PetscCall(PetscLogEventEnd(ctx->event[5], 0, 0, 0, 0));
  /* initialize u */
  PetscCall(DMCreateGlobalVector(dm, &u));
  PetscCall(PetscObjectSetName((PetscObject)u, "u"));
  ctx->sequence_number = 0;
  initu[0] = maxwellian;
  PetscCall(
      DMProjectFunction(dm, 0.0, initu, (void **)mctxs, INSERT_ALL_VALUES, u));
  /* create solver */
  PetscCall(PetscLogEventBegin(ctx->event[1], 0, 0, 0, 0));
  PetscCall(TSCreate(PETSC_COMM_WORLD, &ts));
  PetscCall(TSSetSolution(ts, u));
  PetscCall(TSSetDM(ts, dm));
  PetscCall(TSSetType(ts, TSBEULER));
  PetscCall(TSSetMaxTime(ts, 1.0));
  PetscCall(TSSetExactFinalTime(ts, TS_EXACTFINALTIME_STEPOVER));
  PetscCall(TSSetTimeStep(ts, 0.01));
  PetscCall(TSSetMaxSteps(ts, 1));
  PetscCall(DMTSSetBoundaryLocal(dm, DMPlexTSComputeBoundary, &ctx));
  PetscCall(DMTSSetIFunctionLocal(dm, DMPlexTSComputeIFunctionFEM, &ctx));
  PetscCall(DMTSSetIJacobianLocal(dm, DMPlexTSComputeIJacobianFEM, &ctx));
  // setup solver - default MG
  PetscCall(TSGetSNES(ts, &snes));
  PetscCall(SNESGetKSP(snes, &ksp));
  PetscCall(KSPGetPC(ksp, &pc));
  PetscCall(PCSetType(pc, PCMG));
  PetscCall(PCMGSetLevels(pc, ctx->nlevels, NULL));
  PetscCall(PCMGSetType(pc, PC_MG_MULTIPLICATIVE));
  PetscCall(PCMGSetGalerkin(pc, PC_MG_GALERKIN_BOTH));
  PetscCall(PCMGSetCycleType(pc, PC_MG_CYCLE_V));
  PetscCall(PCMGSetNumberSmooth(pc, 2));
  PetscCall(TSSetFromOptions(ts));
  PetscCall(TSMonitorSet(ts, Monitor, ctx, NULL));
  PetscCall(PetscLogEventEnd(ctx->event[1], 0, 0, 0, 0));
  PetscBool ispcmg;
  PetscCall(PetscObjectTypeCompare((PetscObject)pc, PCMG, &ispcmg));
  if (ispcmg) {
    DM cdm, fdm = dm, dmhier[16];
    PetscInt nlevels = 0;
    while (fdm) {              // get coarse DM and count
      dmhier[nlevels++] = fdm; // 0 is fine grid
      PetscCall(DMGetCoarseDM(fdm, &cdm));
      fdm = cdm;
    }
    PetscCall(PCMGSetLevels(pc, nlevels, NULL));
    PetscCall(PCMGSetType(pc, PC_MG_MULTIPLICATIVE)); // remove
    PetscCall(PCMGSetGalerkin(pc, PC_MG_GALERKIN_PMAT));
    PetscCall(PetscInfo(dm, "Set up MG with %d levels\n", (int)nlevels));
    for (PetscInt k = 1, cidx = nlevels - 1; k < nlevels; k++, cidx--) {
      Mat R;
      PetscInt M, N;
      cdm = dmhier[cidx];
      fdm = dmhier[cidx - 1];
      PetscCall(DMCreateInterpolation(cdm, fdm, &R, NULL));
      // PetscCall(MatFilter(R, PETSC_SQRT_MACHINE_EPSILON, PETSC_TRUE,
      // PETSC_TRUE));
      //  PetscCall(MatEliminateZeros(R));
      PetscCall(MatViewFromOptions(R, NULL, "-tok_interp_mat_view"));
      // set R
      PetscCall(PCMGSetInterpolation(pc, k, R));
      PetscCall(MatGetSize(R, &M, &N));
      PetscCall(PetscInfo(dm, "MG level id %d) R is %d x %d\n", (int)cidx,
                          (int)M, (int)N));
      PetscCall(MatDestroy(&R));
      cdm = fdm;
    }
  }
  { // we could resetup stuff every -ts_max_steps steps, or use
    // -pc_gamg_reuse_interpolation false
    TSConvergedReason tsreason = TS_CONVERGED_ITERATING;
    PetscInt ds, stepi = 0;
    PetscReal t = 0.0, dt = 0;
    while (tsreason != TS_CONVERGED_TIME && tsreason != TS_CONVERGED_ITS) {
      Mat J;
      PetscCall(PetscLogEventBegin(ctx->event[2], 0, 0, 0, 0));
      /* solve */
      //PetscCall(TSSolve(ts, u));
      PetscErrorCode ierr = TSSolve(ts, u);
      PetscCall(PetscLogEventEnd(ctx->event[2], 0, 0, 0, 0));
      PetscCall(SNESGetJacobian(snes, &J, NULL, NULL, NULL));
      PetscCall(MatViewFromOptions(J, NULL, "-tor_jac_view"));
      if (ierr) {return ierr;}
      // inc
      PetscCall(TSGetConvergedReason(ts, &tsreason));
      PetscCall(TSGetStepNumber(ts, &ds));
      PetscCall(TSGetTimeStep(ts, &dt));
      PetscCall(TSGetTime(ts, &t));
      stepi += ds;
      PetscCall(PetscPrintf(PETSC_COMM_WORLD,
                            "step %d) time %e. reason %s dt=%e\n", (int)stepi,
                            t, TSConvergedReasons[tsreason], dt));
      /* inc */
      PetscCall(TSSetStepNumber(ts, 0));
    }
  }
  // post solve
  PetscCall(PetscLogEventBegin(ctx->event[3], 0, 0, 0, 0));
  if (ispcmg) {
    PetscReal gc = 0, oc = 0;
    PetscCall(PCMGGetGridComplexity(pc, &gc, &oc));
    PetscCall(PetscInfo(dm, "      Complexity:    grid = %g    operator = %g\n",
                        (double)gc, (double)oc));
    PC_MG *mg = (PC_MG *)pc->data;
    PC_MG_Levels **mglevels = mg->levels;
    PetscInt levels = mglevels ? mglevels[0]->levels : 0, i, N;
    Mat Pmat;
    MatInfo info;
    PetscCall(KSPGetOperators(mglevels[levels - 1]->smoothd, NULL, &Pmat));
    PetscCall(MatGetInfo(Pmat, MAT_GLOBAL_SUM, &info)); /* global reduction */
    for (i = 0; i < levels; i++) {
      PetscCall(KSPGetOperators(mglevels[i]->smoothd, NULL, &Pmat));
      PetscCall(MatGetInfo(Pmat, MAT_GLOBAL_SUM, &info)); /* global reduction */
      PetscCall(MatGetSize(Pmat, &N, NULL));
      PetscCall(PetscInfo(
          dm, " %d) n = %d nnz = %g nnz/row = %d fill ratio = %g\n", (int)i,
          (int)N, (double)info.nz_used, (int)(info.nz_used / (double)N),
          (double)info.fill_ratio_needed));
      if (i == 1) {
        PetscCall(PCMGGetInterpolation(pc, i, &Pmat));
        PetscCall(MatViewFromOptions(Pmat, NULL, "-tok_Rlast_mat_view"));
      }
      if (i == levels - 1 && levels > 1) {
        PetscCall(PCMGGetInterpolation(pc, i, &Pmat));
        PetscCall(MatViewFromOptions(Pmat, NULL, "-tok_Rfine_mat_view"));
      }
      if (i == 0) {
        PetscCall(KSPGetOperators(mglevels[i]->smoothd, NULL, &Pmat));
        PetscCall(MatViewFromOptions(Pmat, NULL, "-tok_coarse_mat_view"));
      }
    }
  }
  // debug: print anisotropic vector
  if (ctx->print) {
    DM celldm;
    PetscFE fe;
    Vec uu;
    char prefix[] = "b0_";
    PetscErrorCode (*initu[1])(PetscInt, PetscReal, const PetscReal[], PetscInt,
                               PetscScalar[], void *);
    PetscCall(DMClone(dm, &celldm));
    PetscCall(PetscFECreateDefault(PETSC_COMM_WORLD, ctx->dim, ctx->dim,
                                   PETSC_FALSE, prefix, -1, &fe));
    PetscCall(PetscObjectSetName((PetscObject)fe, "b0"));
    PetscCall(PetscFEViewFromOptions(fe, NULL, "-fe_view"));
    PetscCall(DMSetField(celldm, 0, NULL, (PetscObject)fe));
    PetscCall(DMCreateDS(celldm));
    PetscCall(DMCreateGlobalVector(celldm, &uu));
    PetscCall(PetscObjectSetName((PetscObject)uu, "uu"));
    initu[0] = b_vec;
    PetscCall(
        DMProjectFunction(celldm, 0.0, initu, NULL, INSERT_ALL_VALUES, uu));
    PetscCall(DMViewFromOptions(celldm, NULL, "-b0_dm_view"));
    PetscCall(VecViewFromOptions(uu, NULL, "-b0_vec_view"));
    PetscCall(PetscFEDestroy(&fe));
    PetscCall(DMDestroy(&celldm));
    PetscCall(VecDestroy(&uu));
  }
  // cleanup
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&u));
  PetscCall(TSDestroy(&ts));
  PetscCall(PetscLogEventEnd(ctx->event[3], 0, 0, 0, 0));

  PetscCall(PetscFinalize());
  return 0;
}

/*TEST

   testset:
     args: -dm_plex_dim 2 -dm_plex_simplex 0
-tok_toroidal_refine 0 -tok_poloidal_refine 3 -tok_coarse_toroidal_faces 4
-potential_petscspace_degree 3 -snes_type ksponly -ksp_type cg -ksp_monitor
-mg_levels_esteig_ksp_type cg -mg_levels_pc_type jacobi -ts_type arkimex
-ts_arkimex_type 1bee -ts_rtol 1e-6 -ksp_converged_reason -ts_monitor
-ts_max_steps 1 -ts_adapt_monitor requires: !complex hdf5 nsize: 1 test: suffix:
2d args: -tok_dim 2 -dm_plex_box_faces 1,1

     test:
       suffix: 3d
       args: -tok_dim 3 -tok_toroidal_refine 4 -ref_dm_plex_dim 2
-ref_dm_plex_simplex 0 -tok_poloidal_refine 1 -tok_anisotropic 1e-9
-tok_uniform_poloidal_refine 1 -tok_jac_view ascii::ascii_info
-ref_petscpartitioner_type simple    -tok_radius_inflation 1.1 -tok_theta 0.02
-tok_print 1 -ts_dt 2e-4 -tok_dm_view_aux

TEST*/
